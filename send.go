package main

import (
	amqp "github.com/streadway/amqp"
	"log"
)

func failOnError2(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func send_messages(host string, port string, login string, password string, text string, count int) {
	conn, err := amqp.Dial("amqp://" + login + ":" + password + "@" + host + ":" + port + "/")
	failOnError2(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	failOnError2(err, "Failed to open a channel")
	defer ch.Close()

	q, err := ch.QueueDeclare(
		"test", // name
		false,  // durable
		false,  // delete when unused
		false,  // exclusive
		false,  // no-wait
		nil,    // arguments
	)
	failOnError2(err, "Failed to declare a queue")

	body := text
	for i := 0; i < count; i++ {

		err = ch.Publish(
			"",     // exchange
			q.Name, // routing key
			false,  // mandatory
			false,  // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(body),
			})
	}
	failOnError2(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s", body)
}
