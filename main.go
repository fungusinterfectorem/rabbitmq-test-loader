package main

import "time"

func main() {
	conf := NewConfig("./config.yaml")
	send_messages(conf.Server.Host, conf.Server.Port, conf.User.Login, conf.User.Password, conf.Message.Text, conf.Message.Count)
	wait(time.Duration(conf.Time.Timeout))
	receive_messages(conf.Server.Host, conf.Server.Port, conf.User.Login, conf.User.Password)
}
