### RabbitMQ load testing application

##### Install
###### 1. Install Golang with apt/yum
###### - on Ubuntu: `apt install golang`
###### - on CentOs/RHEL: `yum install golang`
###### or 
###### 1.1 Install Golang manually 
###### - download last version of golang from official [site](https://golang.org/dl/) in *.tar.gz archive
###### - run `rm -rf /usr/local/go && tar -C /usr/local -xzf go1.16.6.linux-amd64.tar.gz`
###### - run `export PATH=$PATH:/usr/local/go/bin`
###### 2. Build application
###### - run `go build -o [filename]`
###### 3. Change Config
###### - in config.yaml 
###### - host (select address of RabbitMQ instance)
###### - port (select port of RabbitMq instance)
###### - timeout (timeout for receiver start)
###### - text (write you message)
###### - count (count of messages)
###### 4. Run application
###### - run `./[filename]`
