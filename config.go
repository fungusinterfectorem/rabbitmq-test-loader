package main

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	Server struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
	} `yaml:"server"`
	Time struct {
		Timeout int32 `yaml:"timeout"`
	} `yaml:"time"`
	User struct {
		Login    string `yaml:"login"`
		Password string `yaml:"password"`
	} `yaml:"user"`
	Message struct {
		Text  string `yaml:"text"`
		Count int    `yaml:"count"`
	} `yaml:"message"`
}

func NewConfig(configPath string) *Config {

	config := &Config{}
	file, err := os.Open(configPath)
	if err != nil {
		return nil
	}
	defer file.Close()
	d := yaml.NewDecoder(file)
	if err := d.Decode(&config); err != nil {
		return nil
	}
	return config
}
